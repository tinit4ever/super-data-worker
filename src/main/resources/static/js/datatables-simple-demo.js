window.addEventListener('DOMContentLoaded', event => {
    const datatablesSimple = document.getElementById('datatablesSimple');
    if (datatablesSimple) {
        fetch('http://localhost:8080/api/customers')
            .then(response => response.json())
            .then(data => {
                if (data.status === 'OK') {
                    const tableBody = datatablesSimple.querySelector('tbody');
                    tableBody.innerHTML = '';
                    data.data.forEach(customer => {
                        const newRow = document.createElement('tr');
                        newRow.innerHTML = `
                            <td>${customer.id}</td>
                            <td>${customer.firstName}</td>
                            <td>${customer.lastName}</td>
                            <td>${customer.address}</td>
                            <td>${customer.age}</td>
                            <td>${customer.status}</td>
                            <td></td>
                        `;
                        tableBody.appendChild(newRow);
                    });
                    new simpleDatatables.DataTable(datatablesSimple);
                } else {
                    console.error('Failed to fetch data from the API');
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }
});