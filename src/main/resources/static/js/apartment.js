fetch('http://localhost:8080/api/apartments')
.then(response => response.json())
.then(data => {
    const tableBody = document.getElementById('apartmentTableBody');
    data.data.forEach(apartment => {
        const row = document.createElement('tr');
        row.innerHTML = `
                    <td>${apartment.id}</td>
                    <td>${apartment.address}</td>
                    <td>${apartment.rentalPrice}</td>
                    <td>${apartment.numberOfRooms}</td>
                `;
        tableBody.appendChild(row);
    });
})
.catch(error => console.error('Error fetching data:', error));
