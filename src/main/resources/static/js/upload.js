$(document).ready(function() {
    function handleFormSubmission(formId, apiUrl) {
        $(formId).submit(function(event) {
            event.preventDefault();

            var formData = new FormData($(this)[0]);

            $.ajax({
                url: apiUrl,
                type: 'POST',
                data: formData,
                async: false,
                success: function(response) {
                    alert('Status: ' + response.status + '\nMessage: ' + response.message + 
                    '\nSuccess: ' + response.success + '\nFailed: ' + response.faild);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
    }

    handleFormSubmission("#uploadCustomerForm", "/api/customers/import");
    handleFormSubmission("#uploadApartmentForm", "/api/apartments/import");
    handleFormSubmission("#uploadContractForm", "/api/contracts/import");
});