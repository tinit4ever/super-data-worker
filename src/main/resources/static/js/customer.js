fetch('http://localhost:8080/api/customers')
    .then(response => response.json())
    .then(data => {
        const tableBody = document.getElementById('customerTableBody');
        data.data.forEach(customer => {
            const row = document.createElement('tr');
            row.innerHTML = `
                        <td>${customer.id}</td>
                        <td>${customer.firstName}</td>
                        <td>${customer.lastName}</td>
                        <td>${customer.address}</td>
                        <td>${customer.age}</td>
                        <td>${customer.status}</td>
                    `;
            tableBody.appendChild(row);
        });
    })
    .catch(error => console.error('Error fetching data:', error));

// Define variables for pagination
// const pageSize = 10; // Number of records to fetch per page
// let currentPage = 1;

// // Function to fetch and display customer data for the current page
// function fetchAndDisplayCustomers() {
//     fetch(`http://localhost:8080/api/customers?page=${currentPage}&pageSize=${pageSize}`)
//         .then(response => response.json())
//         .then(data => {
//             const tableBody = document.getElementById('customerTableBody');
//             tableBody.innerHTML = ''; // Clear existing rows

//             data.data.forEach(customer => {
//                 const row = document.createElement('tr');
//                 row.innerHTML = `
//                     <td>${customer.id}</td>
//                     <td>${customer.firstName}</td>
//                     <td>${customer.lastName}</td>
//                     <td>${customer.address}</td>
//                     <td>${customer.age}</td>
//                     <td>${customer.status}</td>
//                 `;
//                 tableBody.appendChild(row);
//             });
//         })
//         .catch(error => console.error('Error fetching customer data:', error));
// }

// // Function to handle pagination
// function handlePagination() {
//     // Implement pagination logic here
//     // For example, you can add "Previous" and "Next" buttons to navigate between pages
// }

// // Initial fetch and display of customers
// fetchAndDisplayCustomers();

// // Example: Add event listeners for pagination buttons
// document.getElementById('prevPageButton').addEventListener('click', () => {
//     if (currentPage > 1) {
//         currentPage--;
//         fetchAndDisplayCustomers();
//     }
// });

// document.getElementById('nextPageButton').addEventListener('click', () => {
//     // Implement logic to determine if there are more pages
//     // and update currentPage accordingly
//     currentPage++;
//     fetchAndDisplayCustomers();
// });
