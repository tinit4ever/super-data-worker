package com.groupthree.superdataworker;

import com.groupthree.superdataworker.model.Customer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SuperDataWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperDataWorkerApplication.class, args);
	}

	@Bean
	Customer getCustomer() {
		return new Customer();
	}
}

