package com.groupthree.superdataworker.model;

public enum Role {
    USER,
    ADMIN
}
