package com.groupthree.superdataworker.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "apartment")
public class Apartment {
    @Id
    private String id;

    private String address;
    private String rentalPrice;
    private int numberOfRooms;

    @JsonIgnore
    @OneToMany(mappedBy = "apartment", cascade = CascadeType.ALL)
    private List<Contract> contracts;

}
