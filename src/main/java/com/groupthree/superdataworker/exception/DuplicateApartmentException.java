package com.groupthree.superdataworker.exception;

public class DuplicateApartmentException extends RuntimeException {
    public DuplicateApartmentException(String message) {
        super(message);
    }
}