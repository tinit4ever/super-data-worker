package com.groupthree.superdataworker.exception;

public class DuplicateContractException extends RuntimeException {
    public DuplicateContractException(String message) {
        super(message);
    }
}