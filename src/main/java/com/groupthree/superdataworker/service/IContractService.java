package com.groupthree.superdataworker.service;

import com.groupthree.superdataworker.exception.DuplicateContractException;
import com.groupthree.superdataworker.model.Apartment;
import com.groupthree.superdataworker.model.Contract;
import com.groupthree.superdataworker.model.Customer;
import com.groupthree.superdataworker.repository.ApartmentRepository;
import com.groupthree.superdataworker.repository.ContractRepository;
import com.groupthree.superdataworker.repository.CustomerRepository;
import com.opencsv.CSVReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.InputStreamReader;

@Service
public class IContractService {

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ApartmentRepository apartmentRepository;

    public List<Contract> showAllContracts() {
        List<Contract> listContracts = new ArrayList<>();
        listContracts = contractRepository.findAll();
        return listContracts;
    }

    public Contract addContract(Contract contract) {
        if (contractRepository.existsById(contract.getId())) {
            throw new DuplicateContractException("Contract with ID is " + contract.getId() + " existed");
        }
        return contractRepository.save(contract);
    }

    public List<Object> importContractsFromCsv(MultipartFile file) throws Exception {
        List<String> duplicateContracts = new ArrayList<>();
        int successToAdd = 0;
        int faildToAdd = 0;
        try (CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
            String[] nextLine;

            boolean isFirstRow = true;
            // SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

            while ((nextLine = reader.readNext()) != null) {
                if (isFirstRow) {
                    isFirstRow = false;
                    continue;
                }

                String id = nextLine[0];
                String customerID = nextLine[1];
                String apartmentID = nextLine[2];
                Date startDate = sdf.parse(nextLine[3]);
                Date endDate = sdf.parse(nextLine[4]);

                Customer customer = customerRepository.findById(customerID).orElse(null);
                Apartment apartment = apartmentRepository.findById(apartmentID).orElse(null);

                if (contractRepository.existsById(id)) {
                    duplicateContracts.add("Contract with ID is " + id + " existed");
                    faildToAdd++;

                } else {
                    Contract contract = new Contract();
                    contract.setId(id);
                    contract.setCustomer(customer);
                    contract.setApartment(apartment);
                    contract.setStartDate(startDate);
                    contract.setEndDate(endDate);
                    contractRepository.save(contract);
                    successToAdd++;
                }

            }

            List<Object> result = new ArrayList<>();
            result.add(duplicateContracts);
            result.add(successToAdd);
            result.add(faildToAdd);
            return result;
        }
    }
}
