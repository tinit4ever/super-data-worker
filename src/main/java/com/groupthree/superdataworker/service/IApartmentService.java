package com.groupthree.superdataworker.service;

import com.groupthree.superdataworker.exception.DuplicateApartmentException;
import com.groupthree.superdataworker.model.Apartment;
import com.groupthree.superdataworker.repository.ApartmentRepository;
import com.opencsv.CSVReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.ArrayList;
import java.io.InputStreamReader;

@Service
public class IApartmentService {

    @Autowired
    private ApartmentRepository apartmentRepository;

    public List<Apartment> showAllApartments() {
        List<Apartment> listApartments = new ArrayList<>();
        listApartments = apartmentRepository.findAll();
        return listApartments;
    }

    public Apartment addApartment(Apartment apartment) {
        if (apartmentRepository.existsById(apartment.getId())) {
            throw new DuplicateApartmentException("Apartment with ID is " + apartment.getId() + " existed");
        }
        return apartmentRepository.save(apartment);
    }

    public List<Object> importApartmentsFromCsv(MultipartFile file) throws Exception {
        List<String> duplicateApartments = new ArrayList<>();
        int successToAdd = 0;
        int faildToAdd = 0;
        try (CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
            String[] nextLine;

            boolean isFirstRow = true;

            while ((nextLine = reader.readNext()) != null) {
                if (isFirstRow) {
                    isFirstRow = false;
                    continue;
                }

                String id = nextLine[0];
                String address = nextLine[1];
                String rentalPrice = nextLine[2];
                int numberOfRooms = Integer.parseInt(nextLine[3]);

                if (apartmentRepository.existsById(id)) {
                    duplicateApartments.add("Apartment with ID is " + id + " existed");
                    faildToAdd++;

                } else {
                    Apartment apartment = new Apartment();
                    apartment.setId(id);
                    apartment.setAddress(address);
                    apartment.setRentalPrice(rentalPrice);
                    apartment.setNumberOfRooms(numberOfRooms);
                    apartmentRepository.save(apartment);
                    successToAdd++;
                }

            }
        
            List<Object> result = new ArrayList<>();
            result.add(duplicateApartments);
            result.add(successToAdd);
            result.add(faildToAdd);
            return result;
        }
    }
}
