package com.groupthree.superdataworker.service;

import com.groupthree.superdataworker.exception.DuplicateCustomerException;
import com.groupthree.superdataworker.model.Customer;
import com.groupthree.superdataworker.repository.CustomerRepository;
import com.opencsv.CSVReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.ArrayList;
import java.io.InputStreamReader;

@Service
public class ICustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> showAllCustomers() {
        List<Customer> listCustomers = new ArrayList<>();
        listCustomers = customerRepository.findAll();
        return listCustomers;
    }

    public Customer addCustomer(Customer customer) {
        if (customerRepository.existsById(customer.getId())) {
            throw new DuplicateCustomerException("Customer with ID is " + customer.getId() + " existed");
        }
        return customerRepository.save(customer);
    }

    public List<Object> importCustomersFromCsv(MultipartFile file) throws Exception {
        List<String> duplicateCustomers = new ArrayList<>();
        int successToAdd = 0;
        int faildToAdd = 0;
        try (CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
            String[] nextLine;

            boolean isFirstRow = true;

            while ((nextLine = reader.readNext()) != null) {
                if (isFirstRow) {
                    isFirstRow = false;
                    continue;
                }

                String id = nextLine[0];
                String firstName = nextLine[1];
                String lastName = nextLine[2];
                String address = nextLine[3];
                int age = Integer.parseInt(nextLine[4]);
                String status = nextLine[5];

                if (customerRepository.existsById(id)) {
                    duplicateCustomers.add("Customer with ID is " + id + " existed");
                    faildToAdd++;

                } else {
                    Customer customer = new Customer();
                    customer.setId(id);
                    customer.setFirstName(firstName);
                    customer.setLastName(lastName);
                    customer.setAddress(address);
                    customer.setAge(age);
                    customer.setStatus(status);
                    customerRepository.save(customer);
                    successToAdd++;
                }

            }
        
            List<Object> result = new ArrayList<>();
            result.add(duplicateCustomers);
            result.add(successToAdd);
            result.add(faildToAdd);
            return result;
        }
    }
}
