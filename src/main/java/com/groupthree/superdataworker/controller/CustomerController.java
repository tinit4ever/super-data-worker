package com.groupthree.superdataworker.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.groupthree.superdataworker.exception.DuplicateCustomerException;
import com.groupthree.superdataworker.model.Customer;
import com.groupthree.superdataworker.response.ResponseObject;
import com.groupthree.superdataworker.response.ResponseReadObject;
import com.groupthree.superdataworker.service.ICustomerService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
@RequestMapping(path = "/api/customers")
public class CustomerController {

    @Autowired
    private ICustomerService customerService;

    @GetMapping("")
    public ResponseEntity<ResponseReadObject> listAllCustomer() {
        try {
            List<Customer> listCustomers = customerService.showAllCustomers();
            return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseReadObject("OK", "Succesfully retrieve customer", listCustomers)
            );
        } catch (Exception e) {
            logger.error("Failed to show customer: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ResponseReadObject("ERROR", "Internal server error", null)
            );
        }
    }

    @PostMapping("/add")
    public ResponseEntity<ResponseObject> addCustomer(@RequestBody Customer customer) {
        try {
            Customer saveCustomer = customerService.addCustomer(customer);
            return ResponseEntity.status(HttpStatus.CREATED).body(
                    new ResponseObject("OK", "Import Customer Succesfully", saveCustomer, 1, 0));
        } catch (DuplicateCustomerException e) {
            logger.error("Failed to import customer: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseObject("ERROR", "Duplicate is found", e.getMessage(), 0, 1)
            );
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("ERROR", "Internal server error", null, 0, 1));
        }
    }

    private static final Logger logger = LogManager.getLogger(CustomerController.class);

    @PostMapping("/import")
    public ResponseEntity<ResponseObject> importCustomer(@RequestParam("file") MultipartFile file) {
        try {
            List<Object> response =
            customerService.importCustomersFromCsv(file);

            logger.info("Import Customer Successfully");
            return ResponseEntity.status(HttpStatus.CREATED).body(
                    new ResponseObject("OK", "Import Customer Successfully", response.get(0), response.get(1), response.get(2)));
        } catch (IOException e) {
            logger.error("Failed to import customer: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("ERROR", "Failed to import customer", null, 0, 0));
        } catch (Exception e) {
            logger.error("Internal server error: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("ERROR", "Internal server error", null, 0, 0));
        }
    }
}