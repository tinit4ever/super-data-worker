package com.groupthree.superdataworker.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.groupthree.superdataworker.exception.DuplicateContractException;
import com.groupthree.superdataworker.model.Contract;
import com.groupthree.superdataworker.response.ResponseObject;
import com.groupthree.superdataworker.response.ResponseReadObject;
import com.groupthree.superdataworker.service.IContractService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
@RequestMapping(path = "/api/contracts")
public class ContractController {

    @Autowired
    private IContractService contractService;

    @GetMapping("")
    public ResponseEntity<ResponseReadObject> listAllContract() {
        try {
            List<Contract> listContracts = contractService.showAllContracts();
            return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseReadObject("OK", "Succesfully retrieve contract", listContracts)
            );
        } catch (Exception e) {
            logger.error("Failed to show contract: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                new ResponseReadObject("ERROR", "Internal server error", listAllContract())
            );
        }
    }

    @PostMapping("/add")
    public ResponseEntity<ResponseObject> addContract(@RequestBody Contract contract) {
        try {
            Contract saveContract = contractService.addContract(contract);
            return ResponseEntity.status(HttpStatus.CREATED).body(
                    new ResponseObject("OK", "Import Contract Succesfully", saveContract, 1, 0));
        } catch (DuplicateContractException e) {
            logger.error("Failed to import contract: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ResponseObject("ERROR", "Duplicate is found", e.getMessage(), 0, 1)
            );
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("ERROR", "Internal server error", null, 0, 1));
        }
    }

    private static final Logger logger = LogManager.getLogger(ContractController.class);

    @PostMapping("/import")
    public ResponseEntity<ResponseObject> importContract(@RequestParam("file") MultipartFile file) {
        try {
            List<Object> response =
            contractService.importContractsFromCsv(file);

            logger.info("Import Contract Successfully");
            return ResponseEntity.status(HttpStatus.CREATED).body(
                    new ResponseObject("OK", "Import Contract Successfully", response.get(0), response.get(1), response.get(2)));
        } catch (IOException e) {
            logger.error("Failed to import contract: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("ERROR", "Failed to import contract", null, 0, 0));
        } catch (Exception e) {
            logger.error("Internal server error: {}", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ResponseObject("ERROR", "Internal server error", null, 0, 0));
        }
    }
}