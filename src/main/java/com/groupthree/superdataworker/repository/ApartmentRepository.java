package com.groupthree.superdataworker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import com.groupthree.superdataworker.model.Apartment;

@Repository
public interface ApartmentRepository extends JpaRepository<Apartment, String> {
    boolean existsById(String id);
    Optional<Apartment> findById(String id);
}
