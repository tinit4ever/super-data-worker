package com.groupthree.superdataworker.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.groupthree.superdataworker.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User>findByEmail(String email);
}
