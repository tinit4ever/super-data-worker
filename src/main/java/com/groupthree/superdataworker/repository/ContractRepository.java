package com.groupthree.superdataworker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.groupthree.superdataworker.model.Contract;

@Repository
public interface ContractRepository extends JpaRepository<Contract, String> {
    boolean existsById(String id);
}