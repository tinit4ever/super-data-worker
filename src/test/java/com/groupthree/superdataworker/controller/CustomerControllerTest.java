package com.groupthree.superdataworker.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.groupthree.superdataworker.SuperDataWorkerApplication;
import com.groupthree.superdataworker.exception.DuplicateCustomerException;
import com.groupthree.superdataworker.model.Customer;
import com.groupthree.superdataworker.service.ICustomerService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = SuperDataWorkerApplication.class)
@TestPropertySource("/application-customer-test.properties")
@AutoConfigureMockMvc
@Transactional
@ExtendWith(SpringExtension.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    //@Autowired
    @MockBean
    private ICustomerService customerService;

    @Test
    @DisplayName("Get all customer using GET method success")
    @WithMockUser(username = "user", roles = "USER")
    public void listAllCustomer() throws Exception {
        Customer customer = new Customer();
        customer.setId("SE01");
        customer.setFirstName("John");
        customer.setLastName("Wick");
        customer.setAge(40);
        customer.setAddress("New York");
        customer.setStatus("Active");

        List<Customer> customers = List.of(customer);

        when(customerService.showAllCustomers()).thenReturn(customers);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/customers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("OK")))
                .andExpect(jsonPath("$.message", is("Succesfully retrieve customer")))
                .andExpect(jsonPath("$.data", hasSize(1)))
                .andReturn();
    }

    @Test
    @DisplayName("Get all customer using GET method returns error")
    @WithMockUser
    public void listAllCustomerReturnsError() throws Exception {
        when(customerService.showAllCustomers()).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/customers"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.status", is("ERROR")))
                .andExpect(jsonPath("$.message", is("Internal server error")))
                .andReturn();
    }

    @Test
    @DisplayName("Add customer successfully")
    @WithMockUser
    public void addCustomerSuccessfully() throws Exception {
        Customer customer = new Customer();
        customer.setId("SE00");
        customer.setFirstName("John");
        customer.setLastName("Wick");
        customer.setAge(40);
        customer.setAddress("New York");
        customer.setStatus("Active");

        when(customerService.addCustomer(customer)).thenReturn(customer);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/customers/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customer))
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("OK")))
                .andExpect(jsonPath("$.message", is("Import Customer Succesfully")))
                .andExpect(jsonPath("$.data.id", is(customer.getId())));
    }

    @Test
    @DisplayName("Add duplicate customer")
    @WithMockUser(username = "user", roles = "USER")
    public void addDuplicateCustomer() throws Exception {
        Customer customer = new Customer();
        customer.setId("SE00");
        customer.setFirstName("John");
        customer.setLastName("Wick");
        customer.setAge(40);
        customer.setAddress("New York");
        customer.setStatus("Active");

        when(customerService.addCustomer(customer)).thenThrow(DuplicateCustomerException.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/customers/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customer))
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("ERROR")))
                .andExpect(jsonPath("$.message", is("Duplicate is found")));
    }

    @Test
    @DisplayName("Add customer failure - internal server error")
    @WithMockUser(username = "user", roles = "USER")
    public void addCustomerFailure() throws Exception {
        Customer customer = new Customer();
        customer.setId("SE00");
        customer.setFirstName("John");
        customer.setLastName("Wick");
        customer.setAge(40);
        customer.setAddress("New York");
        customer.setStatus("Active");

        when(customerService.addCustomer(customer)).thenThrow(new RuntimeException());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/customers/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customer))
                        .with(csrf()))
                .andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
    }

    @Test
    @DisplayName("Import customer.csv file successfully")
    @WithMockUser(username = "user", roles = "USER")
    public void importCustomerCsvSuccessfully() throws Exception {
        String content = "id,firstName,lastName,address,age,status\nSE01,John,Doe,New York,25,Active\n2,Jane,Smith,England,20,Inactive";
        byte[] fileContent = content.getBytes();

        MockMultipartFile multipartFile = new MockMultipartFile("file", "customer.csv", "text/csv", fileContent);

        when(customerService.importCustomersFromCsv(multipartFile)).then(Answers.RETURNS_MOCKS);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/customers/import")
                        .file(multipartFile)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.status", is("OK")))
                .andExpect(jsonPath("$.message", is("Import Customer Successfully")));
    }

    @Test
    @DisplayName("Import customer.csv file failure")
    @WithMockUser(username = "user", roles = "USER")
    public void importCustomerCsvFailure() throws Exception {
        String content = "id,firstName,lastName,address,age,status\nSE01,John,Doe,New York,25,Active\n2,Jane,Smith,England,,Inactive";
        byte[] fileContent = content.getBytes();

        MockMultipartFile multipartFile = new MockMultipartFile("file", "customer.csv", "text/csv", fileContent);

        when(customerService.importCustomersFromCsv(multipartFile)).thenThrow(IOException.class);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/customers/import")
                        .file(multipartFile)
                        .with(csrf()))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.status", is("ERROR")))
                .andExpect(jsonPath("$.message", is("Failed to import customer")))
                .andExpect(jsonPath("$.data", is(nullValue())));
    }

    @Test
    @DisplayName("Import customer.csv file internal server error")
    @WithMockUser(username = "user", roles = "USER")
    public void importCustomerCsvInternalServerError() throws Exception {
        String content = "id,firstName,lastName,address,age,status\nSE01,John,Doe,New York,25,Active\n2,Jane,Smith,England,,Inactive";
        byte[] fileContent = content.getBytes();

        MockMultipartFile multipartFile = new MockMultipartFile("file", "customer.csv", "text/csv", fileContent);

        when(customerService.importCustomersFromCsv(multipartFile)).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/customers/import")
                        .file(multipartFile)
                        .with(csrf()))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.status", is("ERROR")))
                .andExpect(jsonPath("$.message", is("Internal server error")))
                .andExpect(jsonPath("$.data", is(nullValue())));
    }

}
