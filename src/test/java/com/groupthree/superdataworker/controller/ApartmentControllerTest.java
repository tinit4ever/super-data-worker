package com.groupthree.superdataworker.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.groupthree.superdataworker.exception.DuplicateApartmentException;
import com.groupthree.superdataworker.model.Apartment;
import com.groupthree.superdataworker.service.IApartmentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ApartmentController.class)
@AutoConfigureMockMvc
public class ApartmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IApartmentService apartmentService;

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void listAllApartment_ReturnsApartments() throws Exception {
        Apartment apartment = new Apartment("1", "Address", "1000", 2);
        List<Apartment> allApartments = Arrays.asList(apartment);

        when(apartmentService.showAllApartments()).thenReturn(allApartments);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/apartments")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(1)))
                .andExpect(jsonPath("$.data[0].id", is(allApartments.get(0).getId())));
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void testInternalServerErrorForListAllApartment() throws Exception {

        when(apartmentService.showAllApartments()).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/apartments")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }
    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void addApartment_ValidApartment_ReturnsCreated() throws Exception {
        // Create a mock Apartment object
        Apartment apartment = new Apartment("1", "Address", "1000", 2);

        // Convert the Apartment object to JSON
        String apartmentJson = new ObjectMapper().writeValueAsString(apartment);

        mockMvc.perform(post("/api/apartments/add")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(apartmentJson))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void addApartment_InvalidApartment_ReturnsBadRequest() throws Exception {
        // Create an invalid Apartment object with missing required fields
        Apartment apartment = new Apartment();

        // Convert the Apartment object to JSON
        String apartmentJson = new ObjectMapper().writeValueAsString(apartment);

        when(apartmentService.addApartment(any())).thenThrow(DuplicateApartmentException.class);

        mockMvc.perform(post("/api/apartments/add")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(apartmentJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void  testInternalServerErrorForAddApartment() throws Exception {

        when(apartmentService.addApartment(any())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/apartments/add")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\",\"address\":\"Address\",\"price\":\"1000\",\"rooms\":2}"))
                .andExpect(status().isInternalServerError());
    }
    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void importApartments_ValidData_ReturnsOk() throws Exception {
        // Read the contents of the apartments.csv file
        Path filePath = Paths.get("src/test/resources/apartments.csv");
        byte[] fileContent = Files.readAllBytes(filePath);
        List<Apartment> apartments = List.of(
                new Apartment("1", "1 HCM", "1000", 1),
                new Apartment("2", "2 HCM", "2000", 2)
        );
        when(apartmentService.importApartmentsFromCsv(any())).thenReturn(Collections.singletonList(apartments));

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/apartments/import")
                        .file(new MockMultipartFile("file", "apartments.csv", "text/csv", fileContent))
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void importApartments_TestImportFailure() throws Exception {
        // Mock the behavior of the apartmentService.importApartments() method to throw an exception
        when(apartmentService.importApartmentsFromCsv(any())).thenThrow(IOException.class);

        // Create a mock MultipartFile object representing the "apartments.csv" file
        MockMultipartFile file = new MockMultipartFile("file", "apartments.csv", "text/csv", "some data".getBytes());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/apartments/import")
                        .file(file)
                        .with(csrf()))
                .andExpect(status().isInternalServerError())
                .andReturn();

        // Add additional assertions on the result
        int statusCode = ((MvcResult) result).getResponse().getStatus();
        assertEquals(500, statusCode);
    }
    @Test
    @WithMockUser(username = "user", roles = "USER")
    public void importApartments_InternalServerError() throws Exception {
        // Mock the behavior of the apartmentService.importApartments() method to throw an exception
        given(apartmentService.importApartmentsFromCsv(any())).willThrow(new RuntimeException("Internal Server Error"));

        // Create a mock MultipartFile object representing the "apartments.csv" file
        MockMultipartFile file = new MockMultipartFile("file", "apartments.csv", "text/csv", "some data".getBytes());

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/apartments/import")
                        .file(file)
                        .with(csrf()))
                .andExpect(status().isInternalServerError())
                .andReturn();

        // Add additional assertions on the result
        int statusCode = ((MvcResult) result).getResponse().getStatus();
        assertEquals(500, statusCode);
    }

}
