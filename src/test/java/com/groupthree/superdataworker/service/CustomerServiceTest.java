package com.groupthree.superdataworker.service;

import com.groupthree.superdataworker.SuperDataWorkerApplication;
import com.groupthree.superdataworker.exception.DuplicateCustomerException;
import com.groupthree.superdataworker.model.Customer;
import com.groupthree.superdataworker.repository.CustomerRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = SuperDataWorkerApplication.class)
@TestPropertySource("/application-customer-test.properties")
public class CustomerServiceTest {

    @MockBean
    private CustomerRepository customerRepository;

    @Autowired
    private ICustomerService customerService;

    @Test
    @DisplayName("Test showAllCustomers")
    public void showAllCustomers() {
        Customer customer = new Customer("SE01", "John", "Bike", "New York", 34, "Active");

        List<Customer> customers = List.of(customer);

        when(customerRepository.findAll()).thenReturn(customers);

        assertEquals(customers.get(0).getId(), customerService.showAllCustomers().get(0).getId());
    }

    @Test
    @DisplayName("Test addCustomer")
    public void addCustomer() {
        Customer customer = new Customer("SE01", "John", "Bike", "New York", 34, "Active");

        when(customerRepository.save(customer)).thenReturn(customer);

        assertEquals(customer, customerService.addCustomer(customer));
    }

    @Test
    @DisplayName("Test addCustomer with DuplicateCustomerException")
    public void addDuplicateCustomer() {
        Customer existingCustomer = new Customer("SE01", "John", "Doe", "New York", 34, "Active");

        when(customerRepository.existsById(existingCustomer.getId())).thenReturn(true);

        DuplicateCustomerException exception = assertThrows(DuplicateCustomerException.class, () -> customerService.addCustomer(existingCustomer));

        assertEquals("Customer with ID is SE01 existed", exception.getMessage());
    }

    @Test
    @DisplayName("Test importCustomersFromCsv")
    void testImportCustomersFromCsv() throws Exception {
        String content = "id,firstName,lastName,address,age,status\nSE01,John,Doe,New York,25,Active";
        byte[] fileContent = content.getBytes();

        MultipartFile multipartFile = new MockMultipartFile("file", "customer.csv", "text/csv", fileContent);

        when(customerRepository.existsById("SE01")).thenReturn(false);

        Customer savedCustomer = new Customer("SE01", "John", "Doe", "New York", 25, "Active");

        when(customerRepository.save(ArgumentMatchers.any())).thenReturn(savedCustomer);

        List<Object> result = customerService.importCustomersFromCsv(multipartFile);

        assertNotNull(result);
        assertEquals(3, result.size());

        List<String> duplicateCustomers = (List<String>) result.get(0);
        int successToAdd = (int) result.get(1);
        int failedToAdd = (int) result.get(2);

        assertEquals(0, duplicateCustomers.size());
        assertEquals(1, successToAdd);
        assertEquals(0, failedToAdd);
    }

    @Test
    @DisplayName("Test importCustomersFromCsv with duplicate data")
    void testImportCustomersFromCsvWithDuplicateData() throws Exception {
        String content = "id,firstName,lastName,address,age,status\nSE01,John,Doe,New York,25,Active";
        byte[] fileContent = content.getBytes();

        MultipartFile multipartFile = new MockMultipartFile("file", "customer.csv", "text/csv", fileContent);

        when(customerRepository.existsById("SE01")).thenReturn(true);

        Customer savedCustomer = new Customer("SE01", "John", "Doe", "New York", 25, "Active");

        when(customerRepository.save(ArgumentMatchers.any())).thenReturn(savedCustomer);

        List<Object> result = customerService.importCustomersFromCsv(multipartFile);

        assertNotNull(result);
        assertEquals(3, result.size());

        List<String> duplicateCustomers = (List<String>) result.get(0);
        int successToAdd = (int) result.get(1);
        int failedToAdd = (int) result.get(2);

        assertEquals(1, duplicateCustomers.size());
        assertEquals(0, successToAdd);
        assertEquals(1, failedToAdd);
    }

}
