package com.groupthree.superdataworker.ContractTest;

import com.groupthree.superdataworker.controller.ContractController;
import com.groupthree.superdataworker.model.Contract;
import com.groupthree.superdataworker.repository.ContractRepository;
import com.groupthree.superdataworker.service.IContractService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class ContractTest {

    @Mock
    private ContractRepository contractRepository;

    @InjectMocks
    private IContractService contractService;


    @Autowired
    private MockMvc mockMvc;
    @Test
    public void importContractTest() throws Exception {
        Path path = Paths.get("C:\\Users\\ADMIN\\Downloads\\contracts.csv");
        String name = "file";
        String originalFileName = "contracts.csv";
        String contentType = "text/csv";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MockMultipartFile file = new MockMultipartFile(name,
                originalFileName, contentType, content);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("file", originalFileName);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/contracts/import")
                        .file(file)
                        .params(params)
                        .content(content)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
        int status = result.getResponse().getStatus();
        Assert.assertEquals(201, status);
    }

    @Test
    public void showListContractTest() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/contracts")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // Kiểm tra kết quả trả về
        int statusCode = result.getResponse().getStatus();
        Assert.assertEquals(200, statusCode);

        String responseContent = result.getResponse().getContentAsString();
    }

    @Test
    public void importContractWithApartmentTest() throws Exception {
        Path path = Paths.get("C:\\Users\\ADMIN\\Downloads\\contracts.csv");
        String name = "file";
        String originalFileName = "contracts.csv";
        String contentType = "text/csv";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MockMultipartFile file = new MockMultipartFile(name,
                originalFileName, contentType, content);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("file", originalFileName);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/contracts/import")
                        .file(file)
                        .params(params)
                        .content(content)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
        int status = result.getResponse().getStatus();
        Assert.assertEquals(201, status);
        path = Paths.get("C:\\Users\\ADMIN\\Downloads\\apartments.csv");
        originalFileName = "apartments.csv";
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        file = new MockMultipartFile(name,
                originalFileName, contentType, content);
        params = new LinkedMultiValueMap<>();
        params.add("file", originalFileName);
        result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/apartments/import")
                        .file(file)
                        .params(params)
                        .content(content)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
        status = result.getResponse().getStatus();
        Assert.assertEquals(201, status);
    }

    @Test
    public void importContractWithCustomer() throws Exception {
        Path path = Paths.get("C:\\Users\\ADMIN\\Downloads\\contracts.csv");
        String name = "file";
        String originalFileName = "contracts.csv";
        String contentType = "text/csv";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MockMultipartFile file = new MockMultipartFile(name,
                originalFileName, contentType, content);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("file", originalFileName);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/contracts/import")
                        .file(file)
                        .params(params)
                        .content(content)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
        int status = result.getResponse().getStatus();
        Assert.assertEquals(201, status);
        path = Paths.get("C:\\Users\\ADMIN\\Downloads\\customer.csv");
        originalFileName = "customer.csv";
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        file = new MockMultipartFile(name,
                originalFileName, contentType, content);
        params = new LinkedMultiValueMap<>();
        params.add("file", originalFileName);
        result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/customers/import")
                        .file(file)
                        .params(params)
                        .content(content)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
        status = result.getResponse().getStatus();
        Assert.assertEquals(201, status);
    }

    @Test
    public void importContractWithWrongFormatTest() throws Exception {
        Path path = Paths.get("C:\\Users\\ADMIN\\Downloads\\test-cases.xlsx");
        String name = "file";
        String originalFileName = "test-cases.xlsx";
        String contentType = "text/xlsx";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MockMultipartFile file = new MockMultipartFile(name,
                originalFileName, contentType, content);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("file", originalFileName);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/contracts/import")
                        .file(file)
                        .params(params)
                        .content(content)
                        .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().is5xxServerError())
                .andReturn();
        int status = result.getResponse().getStatus();
        Assert.assertEquals(500, status);
    }

}
